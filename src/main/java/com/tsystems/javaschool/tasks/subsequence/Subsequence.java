package com.tsystems.javaschool.tasks.subsequence;

import java.util.HashMap;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if((x==null) || (y==null)){
            throw new IllegalArgumentException();
        }
        HashMap y1 = new HashMap();
        for (int i = 0; i < y.size(); i++) {
            if (!y1.containsKey(y.get(i))) {
                y1.put(y.get(i), i);
            }
        }
        int index = 0;
        for (int i = 0; i < x.size(); i++) {
            if (y1.containsKey(x.get(i))) {
                int temp=(Integer) y1.get(x.get(i));
                if (temp < index) {
                    return false;
                } else {
                    index = temp;
                }
            } else {
                return false;
            }
        }
        return true;
    }
}
