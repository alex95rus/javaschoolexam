package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    static void show(int[][] a){
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
    private int last_stored_size;
    private int last_stored_n;

    private HashMap<Integer, Integer> possible_values;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int n = getSizeN(inputNumbers);
        if (n == -1) {
            throw  new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);
        int index = 0;
        int[][] array = new int[n][2 * n - 1];
        for (int i = 0; i < n; i++) {
            ArrayList<Integer> add_list = new ArrayList<>();
            for (int j = 0; j < n - 1 - i; j++) {
                add_list.add(0);
            }
            for (int j = 0; j < i+1; j++) {
                Integer item = inputNumbers.get(index);
                index++;
                add_list.add(item);
                add_list.add(0);
            }
            for (int j = add_list.size(); j <= 2 * n - 1; j++) {
                add_list.add(0);
            }
            int[] arr=new int[2*n-1];
            for (int j = 0; j <2*n-1 ; j++) {
                arr[j]=add_list.get(j);
            }
            array[i]=arr;
        }
        return array;
    }



    public int getSizeN(List<Integer> inputNumbers) {
        //for speeding up requesting
        if(!check_resolution(inputNumbers.size())){
            throw  new CannotBuildPyramidException();
        }
        if((new HashSet(inputNumbers)).size()!=inputNumbers.size()){
            throw  new CannotBuildPyramidException();
        }
        if(inputNumbers.contains(null)){
            throw  new CannotBuildPyramidException();
        }
        //for speeding up requesting when use one instance a lot
        if (check_ability_to_build(inputNumbers.size())) {
            return possible_values.get(inputNumbers.size());
        }
        return -1;
    }

    public boolean check_resolution(int num){
        long a=2*num;
        for (long i = (long)(Math.sqrt(2*num)-3); i < (long)(Math.sqrt(2*num)+3); i++) {
            if(a==i*(i+1)){
                return true;
            }
        }
        return false;
    }


    public PyramidBuilder() {
        last_stored_size = 0;
        last_stored_n = 0;
        possible_values = new HashMap<>();
    }

    public boolean check_ability_to_build(int size) {
        if (last_stored_size < size) {
            for (int i = last_stored_n + 1; i < size; i++) {
                last_stored_size += i;
                possible_values.put(last_stored_size, i);
                last_stored_n = i;
                if (last_stored_size >= size) {
                    break;
                }
            }
        }
        if (possible_values.containsKey(size)) {
            return true;
        }
        return false;
    }

    public void show_set() {
        List<Integer> list = new ArrayList<Integer>(possible_values.keySet());
        Collections.sort(list);
        System.out.println(list.toString());
        System.out.println(possible_values.toString());
    }

}
