package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        //validation
        if (!validation(statement)) {
            return null;
        }
        try {
            double answer = eval(statement);
            if (isInteger(answer)) {
                return Integer.valueOf((int) Math.round(answer)).toString();
            }
            if (Double.isInfinite(answer)) {
                return null;
            }
            return Double.toString(answer);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean validation(String statement) {

        if (statement == null || statement == "") return false;

        if (statement.contains("..")) {
            return false;
        }
        if (statement.contains("++")) {
            return false;
        }
        if (statement.contains("--")) {
            return false;
        }
        if (statement.contains("**")) {
            return false;
        }
        return check_parentheses(statement);
    }

    public static boolean check_parentheses(String statement) {
        LinkedList<Integer> stack = new LinkedList<>();
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(') {
                stack.push(1);
                relax_stack(stack);
            } else if (statement.charAt(i) == ')') {
                stack.push(2);
                relax_stack(stack);
            }
        }
        return stack.isEmpty();
    }

    private static void relax_stack(LinkedList<Integer> stack) {

        if (stack.size() < 2) {
            return;
        }
        Integer first = stack.pop();
        Integer second = stack.pop();
        if ((first == 2) && (second == 1)) {
            return;
        } else {
            stack.push(second);
            stack.push(first);
        }
    }
    public static boolean isInteger(double d) {
        // Note that Double.NaN is not equal to anything, even itself.
        return (d == Math.floor(d)) && !Double.isInfinite(d);
    }

    public static double eval(final String str) {
        return new Object() {
            int pos = -1, ch;

            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }

            boolean eat(int charToEat) {
                while (ch == ' ') nextChar();
                if (ch == charToEat) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char) ch);
                return x;
            }

            // Grammar:
            // expression = term | expression `+` term | expression `-` term
            // term = factor | term `*` factor | term `/` factor
            // factor = `+` factor | `-` factor | `(` expression `)`
            //        | number | functionName factor | factor `^` factor

            double parseExpression() {
                double x = parseTerm();
                for (; ; ) {
                    if (eat('+')) x += parseTerm(); // addition
                    else if (eat('-')) x -= parseTerm(); // subtraction
                    else if (eat('*')) x *= parseTerm();
                    else if (eat('*')) x /= parseTerm();
                    else return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (; ; ) {
                    if (eat('*')) x *= parseFactor(); // multiplication
                    else if (eat('/')) x /= parseFactor(); // division
                    else return x;
                }
            }

            double parseFactor() {
                if (eat('+')) return parseFactor(); // unary plus
                if (eat('-')) return -parseFactor(); // unary minus

                double x;
                int startPos = this.pos;
                if (eat('(')) { // parentheses
                    x = parseExpression();
                    eat(')');
                } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                } else if (ch >= 'a' && ch <= 'z') { // functions
                    while (ch >= 'a' && ch <= 'z') nextChar();
                    String func = str.substring(startPos, this.pos);
                    x = parseFactor();
                    if (func.equals("sqrt")) x = Math.sqrt(x);
                    else if (func.equals("sin")) x = Math.sin(Math.toRadians(x));
                    else if (func.equals("cos")) x = Math.cos(Math.toRadians(x));
                    else if (func.equals("tan")) x = Math.tan(Math.toRadians(x));
                    else throw new RuntimeException("Unknown function: " + func);
                } else {
                    throw new RuntimeException("Unexpected: " + (char) ch);
                }

                if (eat('^')) x = Math.pow(x, parseFactor()); // exponentiation

                return x;
            }
        }.parse();
    }

}
